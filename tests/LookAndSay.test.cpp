#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <array>
#include <utility>

#include "doctest.h"

#include "LookAndSay.hpp"

/************** DATA ****************/

//20th term
U8Vec Expected20 = {1, 1, 1, 3, 1, 2, 2, 1, 1, 3, 1, 2, 1, 1, 1, 3, 2, 2, 2, 1, 2, 3, 2, 1, 1, 2, 1, 1,
                    1, 3, 1, 2, 1, 1, 1, 2, 1, 3, 1, 1, 1, 2, 1, 3, 2, 1, 1, 2, 3, 1, 1, 3, 2, 1, 3, 2,
                    2, 1, 1, 2, 1, 1, 1, 3, 1, 2, 2, 1, 1, 3, 1, 2, 1, 1, 2, 2, 1, 3, 2, 1, 1, 2, 3, 1,
                    1, 3, 2, 1, 3, 2, 2, 1, 1, 2, 3, 1, 1, 3, 1, 1, 2, 2, 2, 1, 1, 3, 1, 1, 1, 2, 3, 1,
                    1, 3, 3, 2, 2, 1, 1, 2, 1, 1, 1, 3, 1, 2, 2, 1, 1, 3, 1, 2, 1, 1, 1, 3, 2, 2, 1, 1,
                    1, 2, 1, 3, 1, 2, 2, 1, 1, 2, 3, 1, 1, 3, 1, 1, 1, 2, 3, 1, 1, 2, 1, 1, 2, 3, 2, 2,
                    2, 1, 1, 2, 1, 3, 2, 1, 1, 3, 2, 1, 3, 2, 2, 1, 1, 3, 3, 1, 1, 2, 1, 3, 2, 1, 2, 3,
                    1, 2, 3, 1, 1, 2, 1, 1, 1, 3, 1, 1, 2, 2, 2, 1, 1, 2, 1, 3, 2, 1, 1, 3, 3, 1, 1, 2,
                    1, 3, 2, 1, 1, 2, 3, 1, 2, 3, 2, 1, 1, 2, 3, 1, 1, 3, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1,
                    1, 3, 1, 2, 2, 1, 1, 3, 1, 2, 1, 1, 1, 3, 1, 2, 3, 1, 1, 2, 1, 1, 2, 3, 2, 2, 1, 1,
                    1, 2, 1, 3, 2, 1, 1, 3, 2, 2, 2, 1, 1, 3, 1, 2, 1, 1, 3, 2, 1, 1};

U8Vec Input20 = {
    1, 3, 2, 1, 1, 3, 2, 1, 3, 2, 2, 1, 1, 3, 3, 1, 1, 2, 1, 3, 2, 1, 2, 3, 1, 2, 3, 1, 1, 2, 1, 1, 1,
    3, 1, 1, 2, 2, 2, 1, 1, 2, 1, 3, 2, 1, 1, 3, 2, 1, 2, 2, 3, 1, 1, 2, 1, 1, 1, 3, 1, 1, 2, 2, 2, 1,
    1, 2, 1, 1, 1, 3, 1, 2, 2, 1, 1, 3, 1, 2, 1, 1, 1, 3, 2, 2, 2, 1, 1, 2, 1, 3, 2, 1, 1, 3, 2, 1, 3,
    2, 2, 1, 2, 3, 2, 1, 1, 2, 1, 1, 1, 3, 1, 2, 1, 1, 1, 2, 1, 3, 3, 2, 2, 1, 1, 2, 3, 1, 1, 3, 1, 1,
    2, 2, 2, 1, 1, 3, 1, 1, 1, 2, 3, 1, 1, 3, 3, 2, 1, 1, 1, 2, 1, 3, 1, 2, 2, 1, 1, 2, 3, 1, 1, 3, 1,
    1, 1, 2, 3, 1, 1, 2, 1, 1, 1, 3, 3, 1, 1, 2, 1, 1, 1, 3, 1, 2, 2, 1, 1, 2, 1, 3, 2, 1, 1, 3, 2, 1,
    3, 2, 1, 1, 1, 2, 1, 3, 3, 2, 2, 1, 2, 3, 1, 1, 3, 2, 2, 1, 1, 3, 2, 1, 2, 2, 2, 1,
};

std::array expected = {std::pair<U8Vec, U8Vec>{{1}, {1, 1}},
                       std::pair<U8Vec, U8Vec>{{1, 1}, {2, 1}},
                       std::pair<U8Vec, U8Vec>{{2, 1}, {1, 2, 1, 1}},
                       std::pair<U8Vec, U8Vec>{{1, 2, 1, 1}, {1, 1, 1, 2, 2, 1}},
                       std::pair<U8Vec, U8Vec>{{1, 1, 1, 2, 2, 1}, {3, 1, 2, 2, 1, 1}},
                       std::pair<U8Vec, U8Vec>{{3, 1, 2, 2, 1, 1}, {1, 3, 1, 1, 2, 2, 2, 1}},
                       std::pair<U8Vec, U8Vec>{{1, 3, 2, 2, 2, 1}, {1, 1, 1, 3, 3, 2, 1, 1}},
                       std::pair<U8Vec, U8Vec>{Input20, Expected20}};

/************* TESTS ****************/

TEST_CASE("Testing the default LookAndSay")
{
    LookAndSay lns;

    for (const auto &pair : expected)
        CHECK_EQ(lns(pair.first), pair.second);
}

TEST_CASE("Testing the singlethreaded LookAndSay")
{
    for (const auto &pair : expected)
        CHECK_EQ(lookAndSaySingleThreaded(pair.first), pair.second);
}

TEST_CASE("Testing the multithreaded LookAndSay")
{
    LookAndSayMultiThreaded lns;

    for (const auto &pair : expected)
        CHECK_EQ(lns(pair.first), pair.second);
}

TEST_CASE("Comparing that the three classes yield the same results" * doctest::timeout(60))
{
    LookAndSay lns;
    LookAndSayMultiThreaded lnsmt;
    auto lnsst = lookAndSaySingleThreaded;

    U8Vec input = Input20;

    for (size_t i = 0; i < 30; ++i)
    {
        CHECK_EQ(lnsst(input), lnsmt(input));
        CHECK_EQ(lns(input), lnsmt(input));
        input = lnsmt(input);
    }
}
