/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <cassert>

#include "LookAndSayMultiThreaded.hpp"

U8Vec LookAndSayMultiThreaded::operator()(U8View current)
{
    {
        auto split = splitTerm(current, threadCount_);

        for (size_t i = 0; i < threadCount_; ++i)
            threads_[i].setWork(split[i]);
    }
    U8Vec result;
    result.reserve(current.size() * 2);
    {
        for (auto &thread : threads_)
        {
            thread.finish();
            // Get result
            auto &&threadRes = std::move(thread).result();
            result.insert(result.end(), threadRes.begin(), threadRes.end());
        }
    }

    return result;
}

LookAndSayMultiThreaded::LookAndSayMultiThreaded()
    : threads_(threadCount_)
{
}

U8View::iterator LookAndSayMultiThreaded::findNextBoundary(U8View term, size_t idx, size_t split)
{
    if (idx == 0)
        return term.begin();
    else if (idx == split)
        return term.end();

    size_t approx = idx * term.size() / split;
    auto val      = term[approx];
    while (++approx < term.size() && term[approx] == val)
        ;
    if (approx == term.size() - 1)
        return term.end();

    return term.begin() + approx;
}

std::vector<U8View> LookAndSayMultiThreaded::splitTerm(U8View term, size_t splitCount)
{
    std::vector<U8View> res;
    for (size_t i = 0; i < splitCount; ++i)
    {
        auto begin = findNextBoundary(term, i, splitCount);
        auto end   = findNextBoundary(term, i + 1, splitCount);
        res.push_back(U8View(begin, end));
    }
    assert(res.size() == splitCount);
    return res;
}
