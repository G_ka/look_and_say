/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <chrono>
#include <string>

class Timer
{
private:
    using clock = std::chrono::high_resolution_clock;

    std::chrono::time_point<clock> start = clock::now();

    std::string text_{};
    bool working_ = true;

public:
    Timer(const std::string &text);

    void stop();

    ~Timer();
};
