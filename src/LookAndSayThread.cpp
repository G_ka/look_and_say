/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <mutex>

#include "LookAndSayThread.hpp"

void LookAndSayThread::stop()
{
    std::unique_lock<std::mutex> lock(mutex_);
    has_work_ = false;
    exiting   = true;
    lock.unlock();
    cond_var_.notify_all();
    if (thread_.joinable())
        thread_.join();
}

void LookAndSayThread::finish()
{
    std::unique_lock<std::mutex> lock(mutex_);
    cond_var_.wait(lock, [this] { return has_work_ == false; });
}

void LookAndSayThread::setWork(U8View in)
{
    std::unique_lock<std::mutex> lock(mutex_);
    has_work_ = true;
    in_       = in;
    lock.unlock();
    cond_var_.notify_one(); // will notify the thread lock
}

U8Vec LookAndSayThread::result() &&
{
    return std::move(out_);
}

LookAndSayThread::LookAndSayThread()
    : thread_([this] {
        while (!exiting)
        {
            std::unique_lock<std::mutex> lock(mutex_);
            cond_var_.wait(lock, [this] { return has_work_ || exiting; });
            if (exiting)
                break;

            if (in_.size())
                out_ = lookAndSaySingleThreaded(in_);

            has_work_ = false;
            lock.unlock();
            cond_var_.notify_all();
        }
    })
{
}
