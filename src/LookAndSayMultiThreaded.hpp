/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include "LookAndSayForward.hpp"
#include "LookAndSayThread.hpp"

class LookAndSayMultiThreaded
{
public:
    U8Vec operator()(U8View current);

    LookAndSayMultiThreaded();

private:
    size_t threadCount_ = std::thread::hardware_concurrency() * 2;
    std::vector<LookAndSayThread> threads_;

    U8View::iterator findNextBoundary(U8View term, size_t idx, size_t split);
    std::vector<U8View> splitTerm(U8View term, size_t splitCount);
};
