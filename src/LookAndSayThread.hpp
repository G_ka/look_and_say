/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <atomic>
#include <mutex>
#include <thread>

#include "LookAndSayForward.hpp"
#include "LookAndSaySingleThreaded.hpp"

class LookAndSayThread
{
public:
    bool exiting{false};

    void stop();
    void finish();

    void setWork(U8View in);

    U8Vec result() &&;

    LookAndSayThread();
    ~LookAndSayThread() { stop(); }

private:
    bool has_work_ = false;
    std::mutex mutex_;
    std::condition_variable cond_var_;

    U8Vec out_;
    U8View in_;

    std::thread thread_;
};
