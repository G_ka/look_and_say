/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <span>
#include <vector>

using U8Vec  = std::vector<uint8_t>;
using U8View = std::span<const uint8_t>;
