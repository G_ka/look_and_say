/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include "LookAndSayMultiThreaded.hpp"
#include "LookAndSaySingleThreaded.hpp"

class LookAndSay
{
public:
    U8Vec operator()(U8View current);

private:
    static constexpr size_t threadLowerLimit = 100'000;
    LookAndSayMultiThreaded threaded_;
};
