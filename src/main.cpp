#include <array>
#include <iostream>
#include <string>

#include "LookAndSay.hpp"
#include "Timer.hpp"

int main()
{
    size_t count{};
    std::cout << "Combien de termes ?\n";
    std::cin >> count;

    U8Vec res = {1};
    LookAndSay lns;
    Timer globalTimer("Total duration");
    for (size_t i = 0; i < count; ++i)
    {
        Timer localTimer("Calculating: " + std::to_string(i + 1));
        res = lns(res);
    }
    std::cin.get();
}
