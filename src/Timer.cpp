/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>

#include "Timer.hpp"

Timer::Timer(const std::string &text)
    : text_(text)
{
}

void Timer::stop()
{
    auto end      = clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << text_ << " : " << duration << " ms" << std::endl;
    working_ = false;
}

Timer::~Timer()
{
    if (working_)
        stop();
}
