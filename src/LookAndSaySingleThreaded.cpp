/* Copyright (C) 2020 G'k
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "LookAndSaySingleThreaded.hpp"

U8Vec lookAndSaySingleThreaded(U8View in)
{
    assert(in.size());

    U8Vec out;
    out.reserve(in.size() * 2);
    uint8_t counter = 0;
    uint8_t prev    = in[0];
    for (const auto num : in)
    {
        if (num == prev)
        {
            ++counter;
        }
        else
        {
            out.push_back(counter);
            out.push_back(prev);
            counter = 1;
            prev    = num;
        }
    }
    if (counter)
    {
        out.push_back(counter);
        out.push_back(in.back());
    }
    return out;
}
